<?php

namespace App\Validator\Constraints;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Contains XSS Validator.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class ContainsXSSValidator extends ConstraintValidator
{
    protected $attackPatterns = [
        // Match any attribute starting with "on" or xmlns
        '#(<[^>]+[\x00-\x20\"\'\/])(on|xmlns)[^>]*>?#iUu',
        // Match javascript:, livescript:, vbscript: and mocha: protocols
        '!((java|live|vb)script|mocha|feed|data):(\w)*!iUu',
        '#-moz-binding[\x00-\x20]*:#u',
        // Match style attributes
        '#(<[^>]+[\x00-\x20\"\'\/])style=[^>]*>?#iUu',
        // Match unneeded tags
        '#</*(applet|meta|xml|blink|link|style|script|embed|object|iframe|frame|frameset|ilayer|layer|bgsound|title|base)[^>]*>?#i'
    ];

    /** @var  TranslatorInterface $translator */
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->detectXSS($value)) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->addViolation();
        }
    }

    /**
     * Given a string, this function will determine if it potentially an
     * XSS attack and return boolean.
     *
     * @param string $string
     *  The string to run XSS detection logic on
     * @return boolean
     *  True if the given `$string` contains XSS, false otherwise.
     */
    protected function detectXSS($string)
    {
        $contains_xss = FALSE;
        // Skip any null or non string values
        if(is_null($string) || !is_string($string)) {
            return $contains_xss;
        }
        // Keep a copy of the original string before cleaning up
        $orig = $string;
        // URL decode
        $string = urldecode($string);
        // Convert Hexadecimals
        $string = preg_replace('!(&#|\\\)[xX]([0-9a-fA-F]+);?!','chr(hexdec("$2"))', $string);
        // Clean up entities
        $string = preg_replace('!(&#0+[0-9]+)!','$1;',$string);
        // Decode entities
        $string = html_entity_decode($string, ENT_NOQUOTES, 'UTF-8');
        // Strip whitespace characters
        $string = preg_replace('!\s!','',$string);

        foreach($this->attackPatterns as $pattern) {
            // Test both the original string and clean string
            if(preg_match($pattern, $string) || preg_match($pattern, $orig)){
                $contains_xss = TRUE;
            }
            if ($contains_xss === TRUE) return TRUE;
        }
        return FALSE;
    }
}