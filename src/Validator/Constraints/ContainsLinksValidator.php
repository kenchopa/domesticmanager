<?php

namespace App\Validator\Constraints;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Contains Links Validator.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class ContainsLinksValidator extends ConstraintValidator
{
    /** @var string $regex */
    protected $regex;
    /** @var  TranslatorInterface $translator */
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->regex =  "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($this->detectLinks($value)) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->addViolation();
        }
    }

    /**
     * Given a string, this function will determine if it has url's / links in it
     *
     * @param string $string
     *  The string to run links detection logic on
     * @return boolean
     *  True if the given `$string` contains links, false otherwise.
     */
    protected function detectLinks($string)
    {
        if( preg_match($this->regex, $string)){
            return true;
        }

        return false;
    }
}