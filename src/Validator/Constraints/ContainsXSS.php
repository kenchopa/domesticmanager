<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsXSS extends Constraint
{
    public $message = 'kenchopa.security.xss.illegal_char';
}