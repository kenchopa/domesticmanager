<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsLinks extends Constraint
{
    public $message = 'kenchopa.security.links.illegal_char';
}