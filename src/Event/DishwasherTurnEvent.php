<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

class DishwasherTurnEvent extends Event
{
    const NAME = 'dishwasher.turn';

    protected $sorting;

    public function __construct($sorting)
    {
        $this->sorting = $sorting;
    }

    public function getSorting()
    {
        return $this->sorting;
    }
}