<?php

namespace App\Helper;

/**
 * Nonce generator.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class NonceGenerator
{
    /** @var String|null */
    private $nonce;

    /**
     * Generates a nonce value that is later used in script and style policies.
     * @return string
     * @throws \Exception
     */
    public function generate() : string
    {
        // generation occurs only when $this->nonce is still null
        if (!$this->nonce) {
            $this->nonce = bin2hex(random_bytes(20));
        }

        return $this->nonce;
    }
}