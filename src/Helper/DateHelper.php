<?php

namespace App\Helper;

use DateTime;

/**
 * Date helper.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class DateHelper
{
    /**
     * @param DateTime $ago
     * @param bool $full
     * @return string
     */
    public function timeAgo(DateTime $ago = null, $full = false)
    {
        if (null == $ago)
            return null;

        $now = new DateTime;
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}