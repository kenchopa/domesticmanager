<?php

namespace App\Slack;

use App\Entity\User;
use GuzzleHttp\Client;

class DishwasherBot
{
    /** @var Client $client */
    private $client;
    /** @var string $webHook */
    private $webHook;
    /** @var string $channel */
    private $channel;
    /** @var boolean $enabled */
    private $enabled;

    public function __construct($webHook, $channel, $enabled)
    {
        $this->client = new Client([]);
        $this->webHook = $webHook;
        $this->channel = $channel;
        $this->enabled = $enabled;
    }

    /**
     * @param User $dishwasher
     * @param null $channel
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(User $dishwasher, $channel = null)
    {
        if (!$this->enabled)
            return null;

        if ($channel == null)
            $channel = $this->channel;

        $username = 'Domestic Manager';
        $icon = ':ghost:';
        $message = $dishwasher->getUsername() . ' is the chosen one this week';

        $payload = json_encode([
            'channel'    => $channel,
            'text'       => $message,
            'username'   => $username,
            'icon_emoji' => $icon
        ]);

        $response = $this->client->post($this->webHook,[
            'body' => $payload
        ]);

        return $response;
    }
}