<?php

namespace App\Controller\Backend;

use App\Entity\User;
use App\Form\Type\Backend\UserCreateType;
use App\Form\Type\Backend\UserEditType;
use App\Manager\UserManager;
use App\Util\DishwasherSorting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/backend/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="backend_user_index")
     * @param Request $request
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, UserManager $userManager)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_INDEX');

        return $this->render('backend/user/index.html.twig', [
            'entities' => $userManager->findDishwashers(),
        ]);
    }

    /**
     * @Route("/create", name="backend_user_create")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserManager $userManager)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_CREATE');

        $user = new User();

        $form = $this->createUserCreateForm($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user
                ->setPassword($password)
                ->addRole('ROLE_DISHWASHER')
            ;
            $userManager->save($user);

            $this->addFlash('success', 'Your successfully created a new user.');

            return $this->redirectToRoute('backend_user_index');
        }

        return $this->render('backend/user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="backend_user_edit")
     * @param Request $request
     * @param UserManager $userManager
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, UserManager $userManager, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_EDIT');

        /** @var User $user */
        if (null == $user = $userManager->find($id))
            throw $this->createNotFoundException("can't find the user");

        $form = $this->createUserEditForm($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save($user);

            $this->addFlash('success', 'Your successfully edited the user.');

            return $this->redirectToRoute('backend_user_index');
        }

        return $this->render('backend/user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * Creates a form to create a Language entity.
     *
     * @param UserInterface $entity The entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createUserCreateForm(UserInterface $entity)
    {
        $form = $this->createForm(UserCreateType::class, $entity, [
            'action' => $this->generateUrl('backend_user_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class,  [
            'label' => 'Save',
            'attr' => [
                'class' => 'btn btn-success',
            ],
        ]);

        return $form;
    }

    /**
     * Creates a form to create a Language entity.
     *
     * @param UserInterface $entity The entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createUserEditForm(UserInterface $entity)
    {
        $form = $this->createForm(UserEditType::class, $entity, [
            'action' => $this->generateUrl('backend_user_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class,  [
            'label' => 'Save',
            'attr' => [
                'class' => 'btn btn-success',
            ],
        ]);

        return $form;
    }
}