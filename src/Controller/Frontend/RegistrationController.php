<?php

namespace App\Controller\Frontend;

use App\Form\Type\Frontend\UserRegistrationType;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/", name="frontend_user_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserManager $userManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserManager $userManager)
    {
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

                $user
                    ->setPassword($password)
                    ->addRole('ROLE_DISHWASHER')
                ;

                $userManager->save($user);

                $this->addFlash('success', 'Your successfully registered, you can now login.');

                return $this->redirectToRoute('frontend_login');
            } else {
                $this->addFlash('notice', 'Your changes were not valid, please correct the errors and proceed.');
            }
        }

        return $this->render('frontend/registration/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}