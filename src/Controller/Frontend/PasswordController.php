<?php

namespace App\Controller\Frontend;

use App\Form\Model\ChangePassword;
use App\Form\Type\Frontend\UserChangePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordController extends Controller
{
    /**
     * @Route("/secured/password/change", name="frontend_user_change_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $changePasswordModel = new ChangePassword();
        $form = $this->createForm(UserChangePasswordType::class, $changePasswordModel);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $password = $passwordEncoder->encodePassword($user, $changePasswordModel->getNewPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Your successfully changed your password.');

            return $this->redirect($this->generateUrl('frontend_user_profile_show'));
        }

        return $this->render('frontend/password/change_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}