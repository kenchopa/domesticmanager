<?php

namespace App\Controller\Frontend;

use App\Event\DishwasherTurnEvent;
use App\Util\DishwasherSorting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DishwasherController extends Controller
{
    /**
     * @Route("/dishwasher", name="frontend_dishwasher_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DISHWASHER_INDEX');

        return $this->render('frontend/dishwasher/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }
}