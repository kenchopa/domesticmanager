<?php

namespace App\Controller\Frontend;

use App\Form\Type\Frontend\UserProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProfileController extends Controller
{
    /**
     * @Route("/secured/profile", name="frontend_user_profile_show")
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, TokenStorageInterface $tokenStorage)
    {
        $this->denyAccessUnlessGranted('ROLE_PROFILE_SHOW');

        return $this->render('frontend/profile/show.html.twig', [
            'user' => $tokenStorage->getToken()->getUser()
        ]);
    }

    /**
     * @Route("/secured/profile/edit", name="frontend_user_profile_edit")
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, TokenStorageInterface $tokenStorage)
    {
        $this->denyAccessUnlessGranted('ROLE_PROFILE_EDIT');

        $user = $tokenStorage->getToken()->getUser();
        $form = $this->createForm(UserProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Your changes were saved!');

                return $this->redirectToRoute('frontend_user_profile_show');
            } else {
                $this->addFlash('notice', 'Your changes were not valid, please correct the errors and proceed.');
            }
        }

        return $this->render('frontend/profile/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}