<?php

namespace App\Command;

use App\Manager\UserManager;
use Symfony\Component\Console\Output\OutputInterface;

class UserDemoteCommand extends UserRoleCommand
{
    protected static $defaultName = 'user:demote';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName(self::$defaultName)
            ->setDescription('Demote a user by removing a role')
            ->setHelp(<<<'EOT'
The <info>user:demote</info> command demotes a user by removing a role
  <info>php %command.full_name% matthieu ROLE_CUSTOM</info>
  <info>php %command.full_name% --super matthieu</info>
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function executeRoleCommand(UserManager $userManager, OutputInterface $output, string $username, bool $super, $role = null)
    {
        if ($super) {
            $userManager->demote($username);
            $output->writeln(sprintf('User "%s" has been demoted as a simple user. This change will not apply until the user logs out and back in again.', $username));
        } else {
            if ($userManager->removeRole($username, $role)) {
                $output->writeln(sprintf('Role "%s" has been removed from user "%s". This change will not apply until the user logs out and back in again.', $role, $username));
            } else {
                $output->writeln(sprintf('User "%s" didn\'t have "%s" role.', $username, $role));
            }
        }
    }
}