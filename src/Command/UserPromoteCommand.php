<?php

namespace App\Command;

use App\Manager\UserManager;
use Symfony\Component\Console\Output\OutputInterface;

class UserPromoteCommand extends UserRoleCommand
{
    protected static $defaultName = 'user:promote';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName(self::$defaultName)
            ->setDescription('Promotes a user by adding a role')
            ->setHelp(<<<'EOT'
The <info>user:promote</info> command promotes a user by adding a role

  <info>php %command.full_name% kenchopa ROLE_CUSTOM</info>
  <info>php %command.full_name% --super kenchopa</info>
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function executeRoleCommand(UserManager $userManager, OutputInterface $output, string $username, bool $super, $role)
    {
        if ($super) {
            $userManager->promote($username);
            $output->writeln(sprintf('User "%s" has been promoted as a super administrator. This change will not apply until the user logs out and back in again.', $username));
        } else {
            if ($userManager->addRole($username, $role)) {
                $output->writeln(sprintf('Role "%s" has been added to user "%s". This change will not apply until the user logs out and back in again.', $role, $username));
            } else {
                $output->writeln(sprintf('User "%s" did already have "%s" role.', $username, $role));
            }
        }
    }
}