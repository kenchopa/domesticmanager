<?php

namespace App\Command;

use App\Event\DishwasherTurnEvent;
use App\Manager\UserManager;
use App\Util\DishwasherSorting;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DishwasherTurnCommand extends Command
{
    protected static $defaultName = 'dishwasher:turn';

    /** @var EventDispatcherInterface $dispatcher */
    private $dispatcher;
    /** @var UserManager $userManager */
    private $userManager;

    public function __construct(EventDispatcherInterface $dispatcher,
                                UserManager $userManager)
    {
        parent::__construct();
        $this->dispatcher = $dispatcher;
        $this->userManager = $userManager;
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription("Who is the next dishwasher")
            ->setDefinition(array(
                new InputArgument('sorting', InputArgument::REQUIRED, 'The sorting'),
            ))
            ->setHelp(<<<'EOT'
The <info>dishwasher:turn</info> command calculates a new dishwasher based on a sorting:
EOT
            );
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sorting = $input->getArgument('sorting');
        if (!in_array($sorting, DishwasherSorting::getSorting())) {
            throw new \Exception('Sorting is not valid (alpha or random)');
        }

        $event = new DishwasherTurnEvent($sorting);
        $this->dispatcher->dispatch(DishwasherTurnEvent::NAME, $event);

        $dishwasher = $this->userManager->findCurrentDishwasher();

        $output->writeln(sprintf('User "%s" has been the chosen one.', $dishwasher->getUsername()));
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('sorting')) {
            $question = new Question('Please choose a sorting (alpha or random):');
            $question->setValidator(function ($sorting) {
                if (empty($sorting)) {
                    throw new \Exception('Sorting can not be empty');
                }
                if (!in_array($sorting, DishwasherSorting::getSorting())) {
                    throw new \Exception('Sorting is not valid (alpha or random)');
                }

                return $sorting;
            });
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('sorting', $answer);
        }
    }
}