<?php

namespace App\Command;

use App\Manager\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class UserDeactivateCommand extends Command
{
    protected static $defaultName = 'user:deactivate';

    /** @var UserManager $userManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        parent::__construct();
        $this->userManager = $userManager;
    }
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Deactivate a user')
            ->setDefinition(array(
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
            ))
            ->setHelp(<<<'EOT'
The <info>user:activate</info> command deactivates a user (so they won't be able to log in):
  <info>php %command.full_name% kenchopa</info>
EOT
            );
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        if (!$this->userManager->deactivate($username)) {
            $output->writeln(sprintf('User "%s" is not found.', $username));
            return;
        }

        $output->writeln(sprintf('User "%s" has been deactivated.', $username));
    }
    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }
                return $username;
            });
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('username', $answer);
        }
    }
}