<?php

namespace App\EventListener;

use App\Entity\User;
use App\Event\DishwasherTurnEvent;
use App\Manager\UserManager;
use App\Slack\DishwasherBot;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class DishwasherListener
{
    /** @var UserManager $userManager */
    private $userManager;
    /** @var \Swift_Mailer $mailer */
    private $mailer;
    /** @var EngineInterface $templating */
    private $templating;
    /** @var DishwasherBot $dishwasherBot */
    private $dishwasherBot;

    public function __construct(UserManager $userManager,
                                \Swift_Mailer $mailer,
                                \Twig_Environment $templating,
                                DishwasherBot $dishwasherBot)
    {
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->dishwasherBot = $dishwasherBot;
    }

    /**
     * @param DishwasherTurnEvent $event
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onDishwasherTurn(DishwasherTurnEvent $event)
    {
        $this->handleTurn($event);
        $this->handleNotification();
    }

    /**
     * @param DishwasherTurnEvent $event
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function handleTurn(DishwasherTurnEvent $event)
    {
        $this->userManager->handleResetDishwashers();
        $this->userManager->updateTurn($event->getSorting());
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function handleNotification()
    {
        $currentDishwasher = $this->userManager->findCurrentDishwasher();

        $this->handleMail($currentDishwasher);

        $this->handleSlack($currentDishwasher);
    }

    /**
     * @param User $dishwasher
     */
    private function handleMail(User $dishwasher)
    {
        $body = $this->templating->render(
            'emails/dishwasher.html.twig', [
                'dishwasher' => $dishwasher
            ]
        );

        $message = (new \Swift_Message('The chosen one'))
            ->setFrom('no-reply@glue.be')
            ->setTo($dishwasher->getEmail())
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }

    /**
     * @param User $dishwasher
     */
    private function handleSlack(User $dishwasher)
    {
        $this->dishwasherBot->send($dishwasher);
    }
}