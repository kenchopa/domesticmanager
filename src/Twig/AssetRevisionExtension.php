<?php

namespace App\Twig;

class AssetRevisionExtension extends \Twig_Extension
{
    /** @var bool|string $publicRoot */
    private $publicRoot;

    /**
     * AssetRevisionExtension constructor.
     * @param $kernelDir
     */
    public function __construct($kernelDir)
    {
        $this->publicRoot = realpath($kernelDir . '/../public');
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('revision', [$this, 'revisionFilter']),
        ];
    }

    /**
     * @param $asset
     * @return string
     * @throws \Exception
     */
    public function revisionFilter($asset) : string
    {
        $manifestPath = $this->getManifestPath();
        if (!file_exists($manifestPath)) {
            return $asset;
        }

        $paths = json_decode(file_get_contents($manifestPath), true);

        if (!isset($paths[$asset])) {
            return "";
        }

        $assetPath = substr($paths[$asset], 1);

        return $assetPath;
    }

    public function getName() : string
    {
        return 'asset_revision';
    }

    private function getManifestPath() : string
    {
        $manifestPath = $this->publicRoot . '/build/manifest.json';
        return $manifestPath;
    }
}