<?php

namespace App\Twig;

use App\Helper\NonceGenerator;

/**
 * Nonce generator for twig.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class NonceGeneratorExtension extends \Twig_Extension
{
    /** @var NonceGenerator $nonceGenerator */
    private $nonceGenerator;

    public function __construct(NonceGenerator $nonceGenerator)
    {
        $this->nonceGenerator = $nonceGenerator;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('csp_nonce', [$this, 'getNonce']),
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getNonce() : string
    {
        return $this->nonceGenerator->generate();
    }
}