<?php

namespace App\Twig;

use App\Helper\DateHelper;

class DateHelperExtension extends \Twig_Extension
{
    /** @var DateHelper $dateHelper */
    private $dateHelper;

    /**
     * AssetRevisionExtension constructor.
     * @param DateHelper $dateHelper
     */
    public function __construct(DateHelper $dateHelper)
    {
        $this->dateHelper = $dateHelper;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('timeAgo', [$this, 'getTimeAgo']),
        ];
    }

    /**
     * @param \DateTime $ago
     * @param bool $full
     * @return string
     */
    public function getTimeAgo(\DateTime $ago, $full = false) : string
    {
        return $this->dateHelper->timeAgo($ago, $full);
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return 'date_helper';
    }
}