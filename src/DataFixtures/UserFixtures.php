<?php

namespace App\DataFixtures;

use App\Manager\UserManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /** @var UserManager $userManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function load(ObjectManager $manager)
    {
        // create super admin
        $this->userManager->create('sudo', 'gluesudo', 'development@kengy.be', true, true);

        // create dishwashers!
        $alphas = range('A', 'K');
        foreach ($alphas as $alpha) {
            $dishWasher = 'dishwasher'.$alpha;
            $password = 'password'.$alpha;
            $email = $dishWasher.'@kengy.be';
            $this->userManager->create($dishWasher, $password, $email, true, false, ['ROLE_DISHWASHER']);
        }
    }
}