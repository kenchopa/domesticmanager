<?php

namespace App\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Util\DishwasherSorting;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var UserRepository $userRepo */
    private $userRepo;
    /** @var UserPasswordEncoderInterface $passwordEncoder */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager,
                                UserRepository $userRepository,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $entityManager;
        $this->userRepo = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->userRepo->find($id);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->userRepo->findAll();
    }

    /**
     * @return array
     */
    public function findDishwashers()
    {
        $removedWithSudoes = [];
        $dishwashers = $this->userRepo->findByRole('ROLE_DISHWASHER');
        /** @var User $dishwasher */
        foreach ($dishwashers as $dishwasher) {
            if ($dishwasher->hasRole('ROLE_SUPER_ADMIN'))
                continue;

            $removedWithSudoes[] = $dishwasher;
        }
        return $removedWithSudoes;
    }

    /**
     * @param $value
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByUsernameOrEmail(string $value)
    {
        return $this->userRepo->findByUsernameOrEmail($value);
    }

    /**
     * @param string $username
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function activate(string $username)
    {
        if (null == $user = $this->findByUsernameOrEmail($username)) {
            return false;
        }

        return $this->enableOrDisableUser($user, true);
    }

    /**
     * @param string $username
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function deactivate(string $username)
    {
        if (null == $user = $this->findByUsernameOrEmail($username)) {
            return false;
        }

        return $this->enableOrDisableUser($user, false);
    }

    /**
     * @param string $username
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function promote(string $username)
    {
        $this->addRole($username, User::ROLE_SUDO);
    }

    /**
     * @param string $username
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function demote(string $username)
    {
        $this->removeRole($username , User::ROLE_SUDO);
    }

    /**
     * @param string $username
     * @param string $role
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addRole(string $username, string $role)
    {
        $user = $this->findByUsernameOrEmail($username);
        if ($user->hasRole($role)) {
            return false;
        }
        $user->addRole($role);
        return $this->save($user);
    }

    /**
     * @param string $username
     * @param string $role
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function removeRole(string $username, string $role)
    {
        $user = $this->findByUsernameOrEmail($username);
        if (!$user->hasRole($role)) {
            return false;
        }
        $user->removeRole($role);
        return $this->save($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function save(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();

        return true;
    }

    /**
     * @param string $username
     * @param string $plainPassword
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function changePassword(string $username, string $plainPassword)
    {
        if (null == $user = $this->findByUsernameOrEmail($username)) {
            return false;
        }

        $password = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($password);

        return $this->save($user);
    }

    /**
     * Creates a user and returns it.
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @param bool   $active
     * @param bool   $sudo
     *
     * @return UserInterface
     */
    public function create($username, $password, $email, $active, $sudo, $roles = [])
    {
        $user = new User();

        $user->setUsername($username)
            ->setEmail($email)
            ->setPassword($this->passwordEncoder->encodePassword($user, $password))
            ->setEnabled((bool) $active);
        ;

        if ($sudo)
            $user->addRole(User::ROLE_SUDO);

        if (!empty($roles)) {
            foreach ($roles as $role) {
                $user->addRole($role);
            }
        }

        $this->save($user);

        return $user;
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function remove(UserInterface $user)
    {
        $this->em->remove($user);
        $this->em->flush();

        return true;
    }

    /**
     * @param string $sorting
     * @return User
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findNextDishwasher($sorting = DishwasherSorting::SORTING_ALPHA)
    {
        return $this->userRepo->findNextDishwasher($sorting);
    }

    /**
     * @return User
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCurrentDishwasher()
    {
        return $this->userRepo->findCurrentDishwasher();
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countTotalDishwashers()
    {
        return (int)$this->userRepo->countTotalDishwashers();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countTotalDoneDishwashers()
    {
        return (int)$this->userRepo->countTotalDoneDishwashers();
    }

    /**
     * @param string $sorting
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function updateTurn($sorting = DishwasherSorting::SORTING_ALPHA)
    {
        // set current dishwasher turn to false
        if (null !== $currentDishwasher = $this->findCurrentDishwasher()) {
            $currentDishwasher->setMyTurn(false);
            $this->save($currentDishwasher);
        }

        // set next dishwasher turn to true
        if (null !== $nextDishwasher = $this->findNextDishwasher($sorting)) {
            $nextDishwasher
                ->setMyTurn(true)
                ->setDoneDishes(true)
            ;
            $this->save($nextDishwasher);
        }

        return true;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handleResetDishwashers()
    {
        $totalDishwashers = $this->countTotalDishwashers();
        $totalDoneDishwashers = $this->countTotalDoneDishwashers();
        if ($totalDishwashers == $totalDoneDishwashers) {
            $this->userRepo->resetAllDishwashers();
        }
    }

    /**
     * @param User $user
     * @param $flag
     * @return bool
     */
    private function enableOrDisableUser(User $user, $flag)
    {
        $user->setEnabled($flag);
        return $this->save($user);
    }
}