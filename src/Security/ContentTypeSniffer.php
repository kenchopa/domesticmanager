<?php

namespace App\Security;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Content Type Sniffer.
 *
 * Sending the new X-Content-Type-Options response header with the value nosniff
 * will prevent Internet Explorer from MIME-sniffing a response away from the declared content-type.
 * Oh and, that's an HTTP header, not a HTML meta tag option. This header prevents "mime" based attacks
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class ContentTypeSniffer implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // listen to the kernel.response event
        return [KernelEvents::RESPONSE => 'addContentTypeHeaderToResponse'];
    }

    /**
     * Adds the content type header to the response.
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
     */
    public function addContentTypeHeaderToResponse(FilterResponseEvent $event)
    {
        // get the Response object from the event
        $response = $event->getResponse();

        // Disables the content type sniffing for script resources. Forces the browser to only execute script files with valid content type headers.
        // This is a non-standard header from Microsoft
        $response->headers->add(['X-Content-Type-Options' => 'nosniff']);
    }
}