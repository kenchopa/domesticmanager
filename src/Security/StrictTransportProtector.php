<?php

namespace App\Security;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Strict Transport Protector.
 *
 * The HTTP Strict-Transport-Security response header (often abbreviated as HSTS)
 * is a security feature that lets a web site tell browsers that it should only be communicated with using HTTPS, instead of using HTTP.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class StrictTransportProtector implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // listen to the kernel.response event
        return [KernelEvents::RESPONSE => 'addStrictTransportHeaderToResponse'];
    }

    /**
     * Adds the transport security header to the response.
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
     */
    public function addStrictTransportHeaderToResponse(FilterResponseEvent $event)
    {
        // get the Response object from the event
        $response = $event->getResponse();

        // only https usage also for sub domains
        $response->headers->set('strict-transport-security', 'max-age=31536000');
    }
}