<?php

namespace App\Security;

use App\Helper\NonceGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * XSS Protector.
 *
 * Content Security Policy (CSP) is a computer security standard introduced to prevent cross-site scripting (XSS),
 * clickjacking and other code injection attacks resulting from execution of malicious content in the trusted web page context.
 *
 * @author Kengy Van Hijfte <development@kengy.be>
 */
class XSSProtector implements EventSubscriberInterface
{
    /** @var NonceGenerator $nonceGenerator */
    private $nonceGenerator;

    public function __construct(NonceGenerator $nonceGenerator)
    {
        $this->nonceGenerator = $nonceGenerator;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // listen to the kernel.response event
        return [KernelEvents::RESPONSE => 'addCSPHeaderToResponse'];
    }

    /**
     * Adds the Content Security Policy header.
     *
     * @param FilterResponseEvent $event
     * @throws \Exception
     */
    public function addCSPHeaderToResponse(FilterResponseEvent $event)
    {
        // get the Response object from the event
        $response = $event->getResponse();

        // add The HTTP X-XSS-Protection response header is a feature of Internet Explorer, Chrome and Safari that stops pages from loading when they detect reflected cross-site scripting (XSS) attacks.
        $response->headers->set('X-XSS-Protection', '1; mode=block');

        // create a CSP rule, using the nonce generator service
        $nonce = $this->nonceGenerator->generate();
        $cspHeader = "script-src 'nonce-" . $nonce . "' 'strict-dynamic' https: http:; object-src 'none';";

        // set CPS header on the response object
        $response->headers->set("Content-Security-Policy", $cspHeader);
    }
}