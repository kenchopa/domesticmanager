<?php

namespace App\Security;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Click Jacking Protector.
 *
 * Clickjacking (User Interface redress attack, UI redress attack, UI redressing)
 * is a malicious technique of tricking a Web user into clicking on something different from what the user perceives they are clicking on,
 * thus potentially revealing confidential information or taking control of their computer while clicking on seemingly innocuous web pages.
 *
 * @author Kengy Van Hijfte <deveopment@kengy.be>
 */
class ClickJackingProtector implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // listen to the kernel.response event
        return [KernelEvents::RESPONSE => 'addClickJackingPreventionHeaderToResponse'];
    }

    /**
     * Adds the click jacking header to the response.
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
     */
    public function addClickJackingPreventionHeaderToResponse(FilterResponseEvent $event)
    {
        // get the Response object from the event
        $response = $event->getResponse();

        // set x frame options to deny (you can't use this website in a IFrame)
        // deny for all routes
        $response->headers->set('X-Frame-Options', 'DENY');
    }
}