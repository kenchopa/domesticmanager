<?php

namespace App\Util;

final class DishwasherSorting
{
    const SORTING_ALPHA = 'alpha';
    const SORTING_RANDOM = 'random';

    public static function getSorting()
    {
        return [
            self::SORTING_ALPHA  => self::SORTING_ALPHA,
            self::SORTING_RANDOM => self::SORTING_RANDOM,
        ];
    }

}