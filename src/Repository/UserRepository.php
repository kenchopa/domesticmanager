<?php

namespace App\Repository;

use App\Entity\User;
use App\Util\DishwasherSorting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $value
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByUsernameOrEmail(string $value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :value')
            ->orWhere('u.email = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCurrentDishwasher()
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->andWhere('u.myTurn = 1')
            ->setParameter('roles', '%ROLE_DISHWASHER%')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param string $sorting
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findNextDishwasher($sorting = DishwasherSorting::SORTING_ALPHA)
    {
        switch ($sorting) {
            case DishwasherSorting::SORTING_ALPHA:
                $result = $this->createQueryBuilder('u')
                    ->where('u.roles LIKE :roles')
                    ->andWhere('u.doneDishes = false')
                    ->andWhere('u.snoozed = false')
                    ->setParameter('roles', '%ROLE_DISHWASHER%')
                    ->orderBy('u.username', 'asc')
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult()
                ;
                break;
            case DishwasherSorting::SORTING_RANDOM:
                $result = $this->createQueryBuilder('u')
                    ->where('u.roles LIKE :roles')
                    ->andWhere('u.doneDishes = false')
                    ->andWhere('u.snoozed = false')
                    ->setParameter('roles', '%ROLE_DISHWASHER%')
                    ->orderBy('RAND()')
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult()
                ;
                break;
            default:
                $result = null;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function resetAllDishwashers()
    {
        $q = $this->_em->createQuery("update App\Entity\User u set u.doneDishes = 0");

        return $q->execute();
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countTotalDishwashers()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.roles LIKE :roles')
            ->andWhere('u.snoozed = false')
            ->setParameter('roles', '%ROLE_DISHWASHER%')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countTotalDoneDishwashers()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.roles LIKE :roles')
            ->andWhere('u.doneDishes = 1')
            ->andWhere('u.snoozed = false')
            ->setParameter('roles', '%ROLE_DISHWASHER%')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
