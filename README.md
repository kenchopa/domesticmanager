# REQUIREMENTS
MIN PHP7.1

RECOMMENDED PHP7.2

COMPOSER

YARN

> [PHP Modules] 
> calendar Core ctype date dom exif fileinfo filter ftp
> gettext hash iconv json libxml mysqli mysqlnd openssl pcntl pcre PDO
> pdo_mysql Phar posix readline Reflection session shmop SimpleXML
> sockets sodium SPL standard sysvmsg sysvsem sysvshm tokenizer wddx xml
> xmlreader xmlwriter xsl Zend OPcache zlib 
> [Zend Modules] 
> Zend OPcache

# INSTALLATION

    git clone git@bitbucket.org:kenchopa/domesticmanager.git
    cd domesticmanager
    composer install
    
    Or unzip the received ZIP-file.

## DB creation (while installing composer)

    CREATE DATABASE domesticmanager CHARACTER SET utf8 COLLATE utf8_general_ci;

## Parameters
Update db url in .env file (can be found in the root of the project).

    DATABASE_URL=mysql://db_user:db_user_password@127.0.0.1:3306/db_name
  
 Update mail settings (temporarily gmail smtp settings)
		 
    MAILER_URL=gmail://kenchopa.mail:commonwords@localhost
    
 Update slack channel parameters in the config/services.yml file:
 
 The slack webhook can be made through the Slack API documentation https://api.slack.com/incoming-webhooks
 
     slack_webhook: https://hooks.slack.com/services/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 'EXAMPLE DOES NOT WORK
     slack_channel: "#test"
     slack_enabled: true
     
## Update DB

    php bin/console doctrine:schema:update --dump-sql -force

## Load fixtures (create data)

	php bin/console doctrine:fixtures:load

## Install assets

    yarn install
    
    # compile assets once
    yarn run encore dev
	
	# recompile assets automatically when files change
    yarn run encore dev --watch
	
	# compile assets, but also minify & optimize them
	yarn run encore production

# APPLICATION

## General
An application in Symfony 4.0.3 that sends a daily @user message to a Slack user to inform him/her to clean up and fill the dishwasher at the office. 
Also send an email and post a message in the 'parameterized parameter' channel on mondays (scheduled by a cronjob) to inform the team who is the lucky one this week.  

Every week another person is responsible for this task.  

There are 2 sorting orders implemented into this application.

v1 choose the user in an alphabetical sort order.
 
v2: pick a random person.

someone who already had the pleasure to enjoy this task doesn't need to do the job anymore.
When everybody has done the job, then all the dishwashers will receive a reset.

There is also a protected area where users can login or register. 
An update form where an user can be “snoozed” is also in place, thus removing him/her from the active pool of users


## Login

> **ADMIN** 


  username: sudo 

  password: gluesudo

> **DISHWASHERS FROM A -> K** 

  username: dishwasherA 
  
  password: passwordA 

> . . . 

username: dishwasherK 

password: passwordK

## Roles

**ROLE_ADMIN**
**ROLE_DISHWASHER**

##  Commands

    # enable user
    php bin/console user:activate #username#

    # disable user
    php bin/console user:deactivate #username#

	# change password
    php bin/console user:change-password #username#
	
	# create user
    php bin/console user:create
    
    # demote user
    php bin/console user:demote #username# #role#
	
	# promote user
    php bin/console user:promote #username# #role#
	
	# dishwasher alphabetical turn
	php bin/console dishwasher:turn alpha
	
	# dishwasher random turn
	php bin/console dishwasher:turn random

## Week/Day Cron job

The task to determine when a new user needs to do the dishes, will be made by a cron job.

Run every Monday at 5 AM alphabetical

    crontab -e
    0 5 * * 1 /usr/local/bin/php /var/www/domesticmanager/bin/console dishwasher:turn alpha
    
Run every Monday at 5 AM random

    crontab -e
    0 5 * * 1 /usr/local/bin/php /var/www/domesticmanager/bin/console dishwasher:turn random
